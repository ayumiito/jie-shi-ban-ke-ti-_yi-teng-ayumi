<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ito_ayumi</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
	<form action="newMessage" method="post">
			<label for="title">件名</label> <input name="title" id="title" /> <br />
			<label for="text">本文</label> <input name="text" type="text" id="text"/>  <br />
			<label for="category">カテゴリー</label> <input name="category"id="category" /> <br />
			<input type="hidden" name="userId" value="${loginUser.id}" /> <br />
			<input type="submit" value="登録" /> <br />
			<a href="./">戻る</a>
	</form>
	<div class="messages">
			    <c:forEach items="${messages}" var="message">
			            <div class="message">
			                <div class="account-name">
			                    <span class="account"><c:out value="${messages.id}" /></span>
			                    <span class="name"><c:out value="${messages.user_id}" /></span>
			                </div>
			                <div class="text"><c:out value="${message.text}" /></div>
			                <div class="date"><fmt:formatDate value="${messages.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			            </div>
			    </c:forEach>
			</div>
	<div class="copylight">Copyright(c)YourName</div>
</body>
</html>