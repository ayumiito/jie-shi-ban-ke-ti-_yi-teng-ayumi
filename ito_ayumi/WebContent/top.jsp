<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ito_ayumi</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>


<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="usermanagement.jsp">管理画面</a>
				<a href="./">ホーム</a>
				<a href="newMessage">新規投稿</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.userName}" />
					</h2>
				</div>
				<div class="account">
					<c:out value="${loginUser.loginId}" />
					<c:out value="${loginUser.id}" />
				</div>
			</div>
		</c:if>


		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<c:if test="${message.deleteFlag==0}">
					<form action="DeleteMessages" method="post">
							<div class="message">
								<div class="account-name">
									<span class="name"><c:out value="${message.userName}" /></span><br>
									<span class="category"><c:out value="${message.category}" /></span><br>
									<span class="title"><c:out value="${message.title}" /></span><br>
									<span class="text"><c:out value="${message.text}" /></span><br>
								</div>
								<div class="date">
									<fmt:formatDate value="${message.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</div>
							<br><button type="submit" name="delete">削除</button>
							<input type="hidden" name="changevalue" value="1"
								${message.deleteFlag}>
							<input type="hidden" name="id" value="${message.id}">
					</form>
				</c:if>



				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.deleteFlag==0}">
						<form action="DeleteComments" method="post">
							<c:if test="${comment.messageId==message.id}">
								<div class="comment">
									<span class="account"><c:out value="${comment.userId}" /></span><br>
									<span class="text"><c:out value="${comment.text}" /></span><br>
								</div>
								<button type="submit" name="delete">削除</button>
								<input type="hidden" name="changevalue" value="1" ${comment.deleteFlag}>
								<input type="hidden" name="id" value="${comment.id}">
							</c:if>
						</form>
					</c:if>
				</c:forEach>

				<div class="form-area">
					<form action="Comment" method="post">
						<input type="hidden" name="messageId" value="${message.id}" /> <br />
						<input type="hidden" name="userId" value="${loginUser.id}" /> <br />
						<span class="text">コメント</span><br>
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<br /> <input type="submit" value="投稿" /> <br />
					</form>

				</div>
			</c:forEach>

		</div>
		<div class="copylight">Copyright(c)AyumiIto</div>
	</div>
</body>
</html>