package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("post.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(Integer.parseInt(request.getParameter("userId")));

            new MessageService().register(message);



//            String loginId = request.getParameter("loginId");
//            String password = request.getParameter("password");
//
//
//            if (user != null) {
//            	session.setAttribute("loginUser", user);
//            	response.sendRedirect("./");
//
//            } else {
//            	session.setAttribute("errorMessages", messages);
//            	response.sendRedirect("./");
//            }

        response.sendRedirect("./");
    }

}