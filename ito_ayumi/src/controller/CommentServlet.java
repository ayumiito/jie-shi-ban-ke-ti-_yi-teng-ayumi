package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentService;

@WebServlet(urlPatterns = { "/Comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;



    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    		Comment comment = new Comment();
            comment.setText(request.getParameter("text"));
            comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
            comment.setUserId(Integer.parseInt(request.getParameter("userId")));

            new CommentService().register(comment);



            response.sendRedirect("./");
    }

}