package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("id ");
            sql.append(",user_id");
            sql.append(", title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", delete_flag ");
            sql.append(") VALUES (");
            sql.append(" ?"); // id
            sql.append(", ?"); // user_id
            sql.append(", ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?"); // delete_flag
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getId());
            ps.setInt(2, message.getUserId());
            ps.setString(3, message.getTitle());
            ps.setString(4, message.getText());
            ps.setString(5, message.getCategory());
            ps.setInt(6, message.getDeleteFlag());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void getDelete(Connection connection, int changevalue, int id) {
	      PreparedStatement ps = null;
	      try {
	           StringBuilder sql = new StringBuilder();
	           sql.append("UPDATE messages SET delete_flag=? WHERE id=?" );

	           ps = connection.prepareStatement(sql.toString());

	           ps.setInt(1, changevalue);
	           ps.setInt(2, id);



				int count = ps.executeUpdate();
				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

	}


}