package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.user_name as user_name, ");
            sql.append("messages.created_date as created_date, ");
            sql.append("messages.delete_flag as delete_flag ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");
                String user_name = rs.getString("user_name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                String title = rs.getString("title");
                String category = rs.getString("category");
                Date createdDate = rs.getTimestamp("created_date");
                int deleteFlag = rs.getInt("delete_flag");

                UserMessage message = new UserMessage();
                message.setLoginId(login_id);
                message.setUserName(user_name);
                message.setId(id);
                message.setUserId(userId);
                message.setText(text);
                message.setTitle(title);
                message.setCategory(category);
                message.setCreated_date(createdDate);
                message.setDeleteFlag(deleteFlag);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}