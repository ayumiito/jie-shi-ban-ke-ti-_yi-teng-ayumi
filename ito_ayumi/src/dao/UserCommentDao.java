package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comment.id as id, ");
            sql.append("comment.user_id as user_id, ");
            sql.append("comment.message_id as messages_id, ");
            sql.append("comment.text as text, ");
            sql.append("users.user_name as user_name, ");
            sql.append("comment.delete_flag as delete_flag, ");
            sql.append("comment.created_date as created_date ");
            sql.append("FROM comment ");
            sql.append("INNER JOIN messages ");
            sql.append("ON comment.message_id = messages.id ");
            sql.append("INNER JOIN users ");
            sql.append("ON users.id = comment.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
            	String user_name = rs.getString("user_name");
            	int user_id = rs.getInt("user_id");
                int messages_id = rs.getInt("messages_id");
                int id = rs.getInt("id");
                String text = rs.getString("text");
                Date createdDate = rs.getTimestamp("created_date");
                int deleteFlag = rs.getInt("delete_flag");

                UserComment comment = new UserComment();
                comment.setUserName(user_name);
                comment.setId(id);
                comment.setUserId(user_id);
                comment.setMessageId(messages_id);
                comment.setText(text);
                comment.setCreated_date(createdDate);
                comment.setDeleteFlag(deleteFlag);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}