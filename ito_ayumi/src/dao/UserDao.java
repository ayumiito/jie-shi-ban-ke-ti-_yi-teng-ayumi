package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.ComplementUser;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch");
            sql.append(", department");
            sql.append(", stop");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // user_name
            sql.append(", ?"); // branch
            sql.append(", ?"); // department
            sql.append(", ?"); // stop
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user.getId());
            ps.setString(2, user.getLoginId());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getUserName());
            ps.setInt(5, user.getBranch());
            ps.setInt(6, user.getDepartment());
            ps.setBoolean(7, user.getStop());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (login_id = ?) AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            }else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String userName = rs.getString("user_name");
                int branch = rs.getInt("branch");
                int department = rs.getInt("department");
                boolean stop = rs.getBoolean("stop");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setUserName(userName);
                user.setBranch(branch);
                user.setDepartment(department);
                user.setStop(stop);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		}else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", password = ?");
			sql.append(", user_name = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(", stop = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getUserName());
			ps.setLong(4, user.getBranch());
			ps.setLong(5, user.getDepartment());
			ps.setBoolean(6, user.getStop());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void setStatus(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET stop = ? WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, !user.getStop());
			ps.setInt(2, user.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<ComplementUser> getAllComplementUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "   users.*";
			sql += " , branch.name AS branch_name";
			sql += " , department.name AS department_name";
			sql += " FROM";
			sql += "   users";
			sql += " LEFT JOIN";
			sql += "   branch";
			sql += " ON";
			sql += "   users.branch_id = branch.id";
			sql += " LEFT JOIN";
			sql += "   department";
			sql += " ON";
			sql += "   users.department_id = department.id";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ComplementUser> userList = toComplementUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}



	private List<ComplementUser> toComplementUserList(ResultSet rs) throws SQLException {
		List<ComplementUser> ret = new ArrayList<ComplementUser>();
		try {
			while (rs.next()) {
				ComplementUser user = new ComplementUser();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setStop(rs.getBoolean("stop"));
				user.setBranchName(rs.getString("branch_name"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setDepartmentName(rs.getString("department_name"));
				user.setName(rs.getString("name"));
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}
