package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comment ( ");
            sql.append("id ");
            sql.append(", user_id ");
            sql.append(", message_id ");
            sql.append(", text ");
            sql.append(", created_date ");
            sql.append(", updated_date ");
            sql.append(", delete_flag ");
            sql.append(") VALUES ( ");
            sql.append("?"); // user_id
            sql.append(", ?"); // user_id
            sql.append(", ?"); // message_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?"); // delete_flag
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getId());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getMessageId());
            ps.setString(4, comment.getText());
            ps.setInt(5, comment.getDeleteFlag());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	public void getDelete(Connection connection,int changevalue, int id) {

	      PreparedStatement ps = null;
	      try {
	           StringBuilder sql = new StringBuilder();
	           sql.append("UPDATE comment SET delete_flag=? WHERE id=?" );

	           ps = connection.prepareStatement(sql.toString());

	           ps.setInt(1, changevalue);
	           ps.setInt(2, id);



				int count = ps.executeUpdate();
				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}


}
}